/*!
\file tri.c
\autor Jalbert Sylvain
\version 1
\date 19 novembre 2019
\brief le fichier qui contient les définitions de toutes les méthodes relatives aux tri de tableau
*/

#include "tri.h"

/*!
\fn void triInsertion ( int* tint_tab, int int_taille )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 19 novembre 2019
\brief une procédure qui trie un tableau d'entier par insertion
\param tint_tab le pointeur vers la premiere case du tableau à trier
\param int_taille la taille du tableau à trier
*/
void triInsertion(int* tint_tab, int int_taille){
    int i, j;
    int elementInsere;

    for (i = 1; i < int_taille; i++) {
        /* Stockage de la valeur en i */
        elementInsere = tint_tab[i];
        /* Décale les éléments situés avant tint_tab[i] vers la droite
           jusqu'à trouver la position d'insertion */
        for (j = i; j > 0 && tint_tab[j-1] > elementInsere; j--) {
            tint_tab[j] = tint_tab[j-1];
        }
        /* Insertion de la valeur stockée à la place vacante */
        tint_tab[j] = elementInsere;
    }
}

/*!
\fn void triFusion ( int* tint_tab, int int_taille )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 19 novembre 2019
\brief une fonction qui trie un tableau d'entier grâce à la méthode de tri fusion
\param tint_tab le pointeur vers la premiere case du tableau à trier
\param int_taille la taille du tableau à trier
*/
void triFusion(int* tint_tab, int int_taille){
  if(int_taille>1){
    //DECLARATION DES VARIABLES
    int *tint_tab1; //le tableau qui contient la premiere moité du tableau tint_tab
    int *tint_tab2; //le tableau qui contient la seconde moité du tableau tint_tab
    int int_milieu; //la case du milieu du tableau tint_tab

    //INITIALISATION DES VARIABLES
    //le milieu du tableau est la moitié de la taille-1
    int_milieu = int_taille/2;

    //CREATION DES DEUX SOUS TABLEAU
    //tint_tab1 est la premiere partie du tableau
    tint_tab1 = copierSousTableau(tint_tab, 0, int_milieu - 1);
    //tint_tab2 est la seconde partie du tableau
    tint_tab2 = copierSousTableau(tint_tab, int_milieu, int_taille-1);

    //Trier récursivement les deux parties avec l’algorithme du tri fusion.
    triFusion(tint_tab1, int_milieu);
    triFusion(tint_tab2, int_taille-int_milieu);

    //Fusionner les deux tableaux triés en un seul tableau trié
    fusion(tint_tab1, int_milieu, tint_tab2, int_taille-int_milieu, tint_tab);

    //LIBERER LA MEMOIRE
    free(tint_tab1);
    free(tint_tab2);
  }
}

/*!
\fn void triDenombrement ( int* tint_tab, int int_taille )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 20 novembre 2019
\brief une fonction qui trie un tableau d'entier grâce à la méthode de tri par dénombrement
\param tint_tab le pointeur vers la premiere case du tableau à trier
\param int_taille la taille du tableau à trier
*/
void triDenombrement(int* tint_tab, int int_taille){
  if(int_taille>1){
    //DECLARATION DES VARIABLES
    int *tint_tabHist; //le tableau de l'histogramme
    int int_tailleH; //la taille du tableau de l'histogramme
    int int_min; //la valeur minimum du tableau tint_tab
    int int_max; //la valeur maximale du tableau tint_tab
    int int_i; //va parcourir tous les indices du tableau tint_tab
    int int_j; //va parcourir tous les indices du tableau tint_tabHist

    //INITIALISATION DES VARIABLES
    //initialisation du minimum et du maximum du tableau
    minMaxTableau(tint_tab, int_taille, &int_min, &int_max);
    //Definition de la taille du tableau de l'histogramme
    int_tailleH = int_max-int_min+1;
    //le parcours du tableau tint_tabHist commence dès la premiere case
    int_j = 0;
    //le parcours du tableau tint_tab commence dès la premiere case
    int_i = 0;

    //CREATION DU TABLEAU DE L'HISTOGRAMME
    tint_tabHist = creerTableauEntier(int_tailleH);
    //INITIALISATION DU TABLEUA DE L'HISTOGRAMME
    histogramme(tint_tab, int_taille, tint_tabHist, int_tailleH, int_min);

    //Parcours du tableau tint_tab
    while(int_i < int_taille){
      //si la valeur de la case courante de l'histogramme est supperieur à 0
      if(tint_tabHist[int_j] > 0 ){
        //la valeur de la case courante du tableau devient l'indice de l'histogramme
        tint_tab[int_i] = int_j;
        //Décrémenter la valeur de la case courante de l'histogramme
        tint_tabHist[int_j]--;
        //Passage à la case suivante du tableau tint_tab
        int_i++;
      }
      else{
        //Passage à la case suivante du tableau de l'histogramme
        int_j++;
      }
    }

    //LIBERER LA MEMOIRE
    free(tint_tabHist);
  }
}
