/*!
\file tri.h
\autor Jalbert Sylvain
\version 1
\date 19 novembre 2019
\brief le fichier qui contient les déclarations de toutes les méthodes relatives aux tri de tableau
*/

#ifndef __TRI_H_
#define __TRI_H_

#include "tableau.h"
/*!
\fn void triInsertion ( int* tint_tab, int int_taille )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 19 novembre 2019
\brief une fonction qui trier un tableau d'entier par insertion
\param tint_tab le pointeur vers la premiere case du tableau à trier
\param int_taille la taille du tableau à trier
*/
void triInsertion(int* tint_tab, int int_taille);

/*!
\fn void triFusion ( int* tint_tab, int int_taille )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 19 novembre 2019
\brief une fonction qui trie un tableau d'entier grâce à la méthode de tri fusion
\param tint_tab le pointeur vers la premiere case du tableau à trier
\param int_taille la taille du tableau à trier
*/
void triFusion(int* tint_tab, int int_taille);

/*!
\fn void triDenombrement ( int* tint_tab, int int_taille )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 20 novembre 2019
\brief une fonction qui trie un tableau d'entier grâce à la méthode de tri par dénombrement
\param tint_tab le pointeur vers la premiere case du tableau à trier
\param int_taille la taille du tableau à trier
*/
void triDenombrement(int* tint_tab, int int_taille);

#endif
