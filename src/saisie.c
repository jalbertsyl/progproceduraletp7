/*!
\file saisie.c
\autor Jalbert Sylvain
\version 1
\date 19 novembre 2019
\brief le fichier qui contient les définitions de toutes les méthodes relatives à la saisie
*/
#include "saisie.h"

/*!
\fn void viderBuffer ( void )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 19 novembre 2019
\brief une procedure qui va vider le buffer
*/
void viderBuffer(void){
  //DECLARATION DES VARIABLES
  char char_saisie; //le caracère courant dans le buffer

  //INITIALISATION DES VARIABLES
  scanf("%c", &char_saisie);

  //VIDER LE BUFFER
  //tant que le caractère courant est different de \n
  while (char_saisie!='\n') {
    //vider du buffer, le caractère suivant
    scanf("%c", &char_saisie);
  }
}

/*!
\fn int saisirEntier ( void )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 19 novembre 2019
\brief une fonction qui renvoie un entier saisie par l'utilisateur
\return l'entier saisie par l'utilisateur
*/
int saisirEntier(){
  //DECLARATION DES VARIABLES
  int int_nbrSaisi; //est le nombre qui sera saisie

  //test de saisie -> on boucle tant que la saisie est bonne
  while (!scanf("%d", &int_nbrSaisi)) {
    //on vide le buffer pour eviter une boucle infini
    viderBuffer();
    //message d'erreur de saisie
    printf("Erreur lors de la saisie ! Veuillez entrer un entier :");
  }
  return(int_nbrSaisi);
}

/*!
\fn void saisirTableauEntier ( int *tint_tab , int int_taille )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 19 novembre 2019
\brief une procedure qui va remplir un tableau donné, grace à la saisie de l'utilisateur
\param tint_tab le pointeur vers la premiere case du tableau à remplir
\param int_taille la taille du tableau à remplir
*/
void saisirTableauEntier(int *tint_tab, int int_taille){
  //DECLARATION DES VARIABLES
  int int_i; //parcours tous les indice du tableau pour saisir leurs valeurs

  //Parcours de toutes les cases du tableau
  for(int_i = 0 ; int_i < int_taille ; int_i++){
    //Demande de saisie pour la case courante
    printf("Veuillez saisir un entier pour la case n°%d : ", int_i+1);
    //lecture de la valeur de la case courante qui est saisie par l'utilisateur
    tint_tab[int_i] = saisirEntier();
  }
  //On informe l'utilisateur que le tableau est rempli
  printf("Le tableau est rempli !\n");
}
