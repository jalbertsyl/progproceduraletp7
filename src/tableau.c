/*!
\file tableau.c
\autor Jalbert Sylvain
\version 1
\date 19 novembre 2019
\brief le fichier qui contient les définitions de toutes les méthodes relatives aux manipulations de tableau
*/

#include "tableau.h"

/*!
\fn void afficherTableau ( int* tint_tab, int int_taille )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 19 novembre 2019
\brief une fonction qui renvoie un entier saisie par l'utilisateur
\param tint_tab le pointeur vers la premiere case du tableau à afficher
\param int_taille la taille du tableau a afficher
*/
void afficherTableau(int* tint_tab, int int_taille){
  int int_i; //parcours tous les indice du tableau pour afficher la valeur de leurs cases

  //Informer l'utilisateur que l'on affiche le contenu du tableau
  printf("Le tableau contient : ");
  //Parcours du tableau de la case 0 à la case int_taille-1
  for(int_i = 0 ; int_i < int_taille-1 ; int_i++){
    //affichage de la case courante avec le séparateur : ", "
    printf("%d, ", tint_tab[int_i]);
  }
  //affichage de la derniere case avec un retour à la ligne
  printf("%d\n", tint_tab[int_taille-1]);
}

/*!
\fn int *creerTableauEntier ( int int_taille )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 19 novembre 2019
\brief une fonction qui creer un tableau d'entier
\param int_taille la taille du tableau à creer
\return le pointeur de la premiere case du tableau d'entier
*/
int *creerTableauEntier(int int_taille){
  //DECLARATION DES VARIABLES
  int *tint_tab; //le poiteur vers la premiere case du tableau

  //ALLOCATION DE LA MEMOIRE
  tint_tab = malloc(int_taille * sizeof(int));
  //Si l'allocation c'est fini en echec
  if(tint_tab == NULL){
    //Avertir l'utilisateur
    printf("Erreur d'allocation mémoire !");
    //Quitter le programme avec un message d'erreur
    exit(ERREUR_ALLOCATION);
  }

  //retourner l'addresse de la premiere case du tableau, soit : tint_tab
  return(tint_tab);
}

/*!
\fn int* copierSousTableau ( int* tint_src , int int_debut , int int_fin )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 19 novembre 2019
\brief une fonction qui permet de copier dans un tableau tint_dest les valeurs du tableau tint_src allant de l’indice int_debut à l’indice int_fin
\param tint_src le tableau source
\param int_debut le premier indice du tableau source à copier dans le tableau de destination
\param int_fin le dernier indice du tableau source à copier dans le tableau de destination
\return le tableau d'entier tint_dest
*/
int* copierSousTableau(int* tint_src, int int_debut, int int_fin){
  //DECLARATION DES VARIABLES
  int *tint_dest; //pointeur vers la premiere case du tableau de destination qui contiendra la copie
  int int_i; //parcours de l'indice int_debut à l'indice int_fin

  //CREATION DU TABLEAU DE DESTINATION
  tint_dest = creerTableauEntier(int_fin-int_debut+1);

  //COPIER LE SOUS TABLEAU
  //parcour du sous tableau dans le tableau source
  for(int_i = int_debut ; int_i <= int_fin ; int_i++){
    //copie de la case courante
    tint_dest[int_i-int_debut] = tint_src[int_i];
  }

  //RETOURNER LA COPIE
  return(tint_dest);
}

/*!
\fn void fusion ( int* tint_tab1 , int int_taille1 , int* tint_tab2 , int int_taille2 , int* tint_tabRes )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 19 novembre 2019
\brief une procedure qui permet de fusionner deux tableaux triés de façon croissante tint_tab1 et tint_tab2 dans un tableau résultat tint_tabRes qui sera lui aussi trié de façon croissante
\param tint_tab1 le premier tableau d'entier à fusionner
\param int_taille1 la taille du premier tableau d'entier à fusionner
\param tint_tab2 le second tableau d'entier à fusionner
\param int_taille2 la taille du second tableau d'entier à fusionner
\param tint_tabRes le tableau qui contient le résultat de la fusion
*/
void fusion(int* tint_tab1, int int_taille1, int* tint_tab2, int int_taille2, int* tint_tabRes){
  //DECLARATION DES VARIABLES
  int int_i; //parcours les indices du premier tableau
  int int_j; //parcours les indices du deuxieme tableau
  int int_r; //parcours les indices du tableau final

  //INITIALISATION DES VARIABLES
  //les trois tableaux seront parcouru à partir de leurs première case
  int_i = 0;
  int_j = 0;
  int_r = 0;

  //tant que un des indices n'a pas dépassé la taille de son tableau respectif
  while(int_i<int_taille1 && int_j<int_taille2){
    //si la case courante du tableau 1 et superieur à la case courante du tableau 2
    if(tint_tab1[int_i] < tint_tab2[int_j]){
      //la case courante du tableau final prend la valeur de la case courante du tableau 1
      tint_tabRes[int_r] = tint_tab1[int_i];
      //incrementer la case courante du tableau 1
      int_i++;
    }
    //sinon, si la case courante du tableau 1 et inferieur ou égal à la case courante du tableau 2
    else{
      //la case courante du tableau final prnd la valeur de la case courante du tableau 2
      tint_tabRes[int_r] = tint_tab2[int_j];
      //incrementer la case courante du tableau 2
      int_j++;
    }
    //incrementer la case courante du tableau final
    int_r++;
  }

  //COPIE DES VALEURS RESTANTES
  //on fini de copier le tableau 1 dans le tableau final
  while(int_i<int_taille1){
    //la case courante du tableau final prnd la valeur de la case courante du tableau 1
    tint_tabRes[int_r] = tint_tab1[int_i];
    //incrementer la case courante du tableau 1
    int_i++;
    //incrementer la case courante du tableau final
    int_r++;
  }
  //on fini de copier le tableau 2 dans le tableau final
  while(int_j<int_taille2){
    //la case courante du tableau final prnd la valeur de la case courante du tableau 2
    tint_tabRes[int_r] = tint_tab2[int_j];
    //incrementer la case courante du tableau 2
    int_j++;
    //incrementer la case courante du tableau final
    int_r++;
  }
}

/*!
\fn void minMaxTableau ( int* tint_tab , int int_taille , int* int_min , int* int_max )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 20 novembre 2019
\brief une procedure qui recherche les valeurs minimum et maximum du tableau tint_tab
\param tint_tab le tableau qui est parcouru
\param int_taille la taille du tableau qui est parcouru
\param int_min la valeur minimum trouvé
\param int_max la valeur maximum trouvé
*/
void minMaxTableau(int* tint_tab, int int_taille, int* int_min, int* int_max){
  //DECLARATION DES VARIABLES
  int int_i; //va parcourir tous les indices du tableau tint_tab

  //INITIALISATION DES VARIABLES
  //la valeur minimum et la valeur maximum vont etre la premiere valeur du tableau
  *int_min = tint_tab[0];
  *int_max = tint_tab[0];

  //parcours de tous les éléments du tableau sauf la premiere case
  for(int_i = 1 ; int_i < int_taille ; int_i++){
    //si l'élément courant est superieur au maximum
    if(tint_tab[int_i] > *int_max){
      //le maximum devient l'élément courant
      *int_max = tint_tab[int_i];
    }
    else{
      //si l'élément courant est inferieur au minimum
      if(tint_tab[int_i] < *int_min){
        //le minimum devient l'élément courant
        *int_min = tint_tab[int_i];
      }
    }
  }
}

/*!
\fn void histogramme ( int* tint_tab , int int_taille , int* tint_histo , int int_tailleH , int int_min )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 20 novembre 2019
\brief une procedure qui permet de déterminer la fréquence d’apparition de chaque élément du tableau tint_tab
\param tint_tab le tableau qui sera traité
\param int_taille la taille du tableau traité
\param tint_histo le tableau de l'histogramme qui contient la fréquence d’apparition de chaque élément du tableau tint_tab
\param int_tailleH la taille du tableau de l'histogramme
\param int_min la valeur minimum dans le tableau tint_tab
*/
void histogramme(int* tint_tab, int int_taille, int* tint_histo, int int_tailleH, int int_min){
  //DECLARATION DES VARIABLES
  int int_i; //va parcourir tous les indices du tableau tint_tab

  //INITIALISATION DE L'HISTOGRAMME
  //parcours de tous les éléments du tableau de l'histogramme
  for(int_i = 0 ; int_i < int_taille ; int_i++){
    tint_histo[int_i] = 0;
  }

  //INCREMENETER L'HISTOGRAMME
  //parcours de tous les éléments du tableau tint_tab
  for(int_i = 0 ; int_i < int_taille ; int_i++){
    tint_histo[tint_tab[int_i]-int_min]++;
  }
}
