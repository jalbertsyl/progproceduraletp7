/*!
\file tableau.h
\autor Jalbert Sylvain
\version 1
\date 19 novembre 2019
\brief le fichier qui contient les déclarations de toutes les méthodes relatives aux manipulations de tableau
*/

#ifndef __TABLEAU_H_
#define __TABLEAU_H_

//onutilise la librairie utile ici pour la gestion de la mémoire et des "exit"
#include <stdlib.h>
//on utilise la librairie utile pour les interactions avec l'utilisateur
#include <stdio.h>

#define ERREUR_ALLOCATION 1

/*!
\fn void afficherTableau ( int* tint_tab, int int_taille )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 11 novembre 2019
\brief une fonction qui renvoie un entier saisie par l'utilisateur
\param tint_tab le pointeur vers la premiere case du tableau à afficher
\param int_taille la taille du tableau a afficher
*/
void afficherTableau(int* tint_tab, int int_taille);

/*!
\fn int *creerTableauEntier ( int int_taille )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 19 novembre 2019
\brief une fonction qui creer un tableau d'entier
\param int_taille la taille du tableau à creer
\return le pointeur de la premiere case du tableau d'entier
*/
int *creerTableauEntier(int int_taille);

/*!
\fn int* copierSousTableau ( int* tint_src , int int_debut , int int_fin )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 19 novembre 2019
\brief une fonction qui permet de copier dans un tableau tint_dest les valeurs du tableau tint_src allant de l’indice int_debut à l’indice int_fin
\param tint_src le tableau source
\param int_debut le premier indice du tableau source à copier dans le tableau de destination
\param int_debut le dernier indice du tableau source à copier dans le tableau de destination
\return le tableau d'entier tint_dest
*/
int* copierSousTableau(int* tint_src, int int_debut, int int_fin);

/*!
\fn void fusion ( int* tint_tab1 , int int_taille1 , int* tint_tab2 , int int_taille2 , int* tint_tabRes )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 19 novembre 2019
\brief une procedure qui permet de fusionner deux tableaux triés de façon croissante tint_tab1 et tint_tab2 dans un tableau résultat tint_tabRes qui sera lui aussi trié de façon croissante
\param tint_tab1 le premier tableau d'entier à fusionner
\param int_taille1 la taille du premier tableau d'entier à fusionner
\param tint_tab2 le second tableau d'entier à fusionner
\param int_taille2 la taille du second tableau d'entier à fusionner
\param tint_tabRes le tableau qui contient le résultat de la fusion
*/
void fusion(int* tint_tab1, int int_taille1, int* tint_tab2, int int_taille2, int* tint_tabRes);

/*!
\fn void minMaxTableau ( int* tint_tab , int int_taille , int* int_min , int* int_max )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 20 novembre 2019
\brief une procedure qui recherche les valeurs minimum et maximum du tableau tint_tab
\param tint_tab le tableau qui est parcouru
\param int_taille la taille du tableau qui est parcouru
\param int_min la valeur minimum trouvé
\param int_max la valeur maximum trouvé
*/
void minMaxTableau(int* tint_tab, int int_taille, int* int_min, int* int_max);

/*!
\fn void histogramme ( int* tint_tab , int int_taille , int* tint_histo , int int_tailleH , int int_min )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 20 novembre 2019
\brief une procedure qui permet de déterminer la fréquence d’apparition de chaque élément du tableau tint_tab
\param tint_tab le tableau qui sera traité
\param int_taille la taille du tableau traité
\param tint_histo le tableau de l'histogramme qui contient la fréquence d’apparition de chaque élément du tableau tint_tab
\param int_tailleH la taille du tableau de l'histogramme
\param int_min la valeur minimum dans le tableau tint_tab
*/
void histogramme(int* tint_tab, int int_taille, int* tint_histo, int int_tailleH, int int_min);

#endif
