/*!
\file tp7.c
\autor Jalbert Sylvain
\version 1
\date 19 novembre 2019
\brief fichier principal du programme qui propose differentes méthodes de tri d'un tableau
*/

#include "tableau.h"
#include "saisie.h"
#include "tri.h"

/*!
\fn int main ( int argc, char∗∗ argv )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 19 novembre 2019
\brief la fonction principale qui gère les choix de l'utilisateur
\param argc nombre d’arguments en entree
\param argv valeur des arguments en entree
\return 0 si tout c'est bien passé
*/
int main (int argc, char** argv){
  int *tint_tab; //le pointeur de la premiere case du tableau
  int int_tailleTab; //la taille du tableau
  int int_choixMenu; //le tri que l'utilisateur a choisi

  //INITIALISATION DE L'ECRAN
  system("clear");

  //L'UTILISATEUR CHOISI LA TAILLE DU TABLEAU
  printf("Veuillez saisir la taile du tableau : ");
  int_tailleTab = saisirEntier();

  //CREATION ET INITIALISATION DU TABLEAU tint_tab
  //allocation en mémoire du tableau d'entier tint_tab
  tint_tab = creerTableauEntier(int_tailleTab);
  //initialisation du tableau tint_tab grace à la saisie de l'utilisateur
  saisirTableauEntier(tint_tab, int_tailleTab);

  //INITIALISATION DE L'ECRAN
  system("clear");

  //AFFICHAGE DU TABLEAU
  afficherTableau(tint_tab, int_tailleTab);

  //AFFICHAGE DU MENU
  printf("Voici les differents tris disponibles :\n\n\t1-Le tri par insersion\n\t2-Le tri fusion\n\t3-Le tri par dénombrement\n\nVeuillez choisir le type de tri à appliquer : ");

  //LECTURE DU CHOIX DE L'UTILISATEUR
  int_choixMenu = saisirEntier();

  //INITIALISATION DE L'ECRAN
  system("clear");

  //si l'utilisateur a selectionné un tri
  if(int_choixMenu>0 && int_choixMenu<4){
    //AFFICHAGE DU TABLEAU AVANT LE TRI
    printf("Avant le tri\n\t");
    afficherTableau(tint_tab, int_tailleTab);

    //EXECUTION DU TRI SELECTIONNE
    switch (int_choixMenu) {
      case 1:
        //Informer du tri qui va etre effectuer
        printf("Tri par insertion...\n");
        //trier le tableau par insertion
        triInsertion(tint_tab, int_tailleTab);
        break;
      case 2:
        //Informer du tri qui va etre effectuer
        printf("Tri fusion...\n");
        //trier le tableau par insertion
        triFusion(tint_tab, int_tailleTab);
        break;
      case 3:
        //Informer du tri qui va etre effectuer
        printf("Tri par dénombrement...\n");
        //trier le tableau par insertion
        triDenombrement(tint_tab, int_tailleTab);
        break;
      default:
        break;
    }

    //AFFICHAGE DU TABLEAU APRES LE TRI
    printf("Après le tri\n\t");
    afficherTableau(tint_tab, int_tailleTab);
  }

  //LIBERER LA MEMOIRE ALOUEE POUR LE TABLEAU
  free(tint_tab);

  //Informer l'utilisateur de la fin du programme
  printf("Au revoir !\n");

  //Fin du programme, Il se termine normalement, et donc retourne 0
  return(0);
}
