/*!
\file saisie.h
\autor Jalbert Sylvain
\version 1
\date 19 novembre 2019
\brief le fichier qui contient les déclarations de toutes les méthodes relatives à la saisie
*/

#ifndef __SAISIE_H_
#define __SAISIE_H_

//on utilise la librairie utile pour les interactions avec l'utilisateur
#include <stdio.h>

/*!
\fn void viderBuffer ( void )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 11 novembre 2019
\brief une procedure qui va vider le buffer
*/
void viderBuffer(void);

/*!
\fn int saisirEntier ( void )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 11 novembre 2019
\brief une fonction qui renvoie un entier saisie par l'utilisateur
\return l'entier saisie par l'utilisateur
*/
int saisirEntier();

/*!
\fn void saisirTableauEntier ( int *tint_tab , int int_taille )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 19 novembre 2019
\brief une procedure qui va remplir un tableau donné, grace à la saisie de l'utilisateur
\param tint_tab le pointeur vers la premiere case du tableau à remplir
\param int_taille la taille du tableau à remplir
*/
void saisirTableauEntier(int *tint_tab, int int_taille);

#endif
